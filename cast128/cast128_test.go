package cast128

import (
	"crypto/cipher"
	"encoding/hex"
	"reflect"
	"testing"
)

type testCase struct {
	input  string
	output []byte
}

var testCases = []testCase{
	{
		"01234567",
		[]byte{0x85, 0xd8, 0x5e, 0x69, 0xe6, 0x84, 0x06, 0x69},
	},
	{
		"0123456789abcdef",
		[]byte{0x85, 0xd8, 0x5e, 0x69, 0xe6, 0x84, 0x06, 0x69, 0x05, 0xd3, 0x12, 0x80, 0x14, 0xc8, 0x86, 0xfb},
	},
	{
		"0123456789abcdefghijklmnopqrstuv",
		[]byte{0x85, 0xd8, 0x5e, 0x69, 0xe6, 0x84, 0x06, 0x69, 0x05, 0xd3, 0x12, 0x80, 0x14, 0xc8, 0x86, 0xfb, 0x83, 0x91, 0x6c, 0x48, 0x19, 0x7b, 0x49, 0x55, 0x66, 0xf6, 0x13, 0xf3, 0x39, 0xf4, 0x49, 0x84},
	},
}

func TestCast128(t *testing.T) {
	// Self-test from libmcrypt.

	plaintext := make([]byte, 8)
	for i := 0; i < len(plaintext); i++ {
		plaintext[i] = byte(i % 256)
	}

	key := make([]byte, 16)
	for i := 0; i < len(key); i++ {
		key[i] = byte((i*2 + 10) % 256)
	}

	ciphertext := make([]byte, 8)

	block, _ := NewCipher(key)
	block.Encrypt(ciphertext, plaintext)

	const want = "434e25460c8c9525"
	got := hex.EncodeToString(ciphertext)
	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}

	plaintext2 := make([]byte, 8)
	block.Decrypt(plaintext2, ciphertext)
	if !reflect.DeepEqual(plaintext, plaintext2) {
		t.Errorf("got %v, want %v", plaintext2, plaintext)
	}

	// Our tests.

	key = make([]byte, 16)
	for i := 0; i < len(key); i++ {
		key[i] = byte(i)
	}

	iv := make([]byte, 8)
	for i := 0; i < len(iv); i++ {
		iv[i] = byte(i)
	}

	block, _ = NewCipher(key)

	for _, tc := range testCases {
		cbc := cipher.NewCBCEncrypter(block, iv)
		text := []byte(tc.input)
		cbc.CryptBlocks(text, text)

		if !reflect.DeepEqual(text, tc.output) {
			t.Errorf("got %v, want %v", text, tc.output)
		}

		dec := cipher.NewCBCDecrypter(block, iv)
		text2 := make([]byte, len(text))
		dec.CryptBlocks(text2, text)

		if !reflect.DeepEqual(text2, []byte(tc.input)) {
			t.Errorf("got %v, want %v", text2, []byte(tc.input))
		}
	}
}
