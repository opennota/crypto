// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package cast128 implements CAST-128 encryption.
package cast128

//go:generate macro internal.go.tmpl internal.go

import (
	"crypto/cipher"
	"strconv"
)

// BlockSize is the CAST-128 block size in bytes.
const BlockSize = 8

type KeySizeError int

func (k KeySizeError) Error() string {
	return "cast128: invalid key size " + strconv.Itoa(int(k))
}

// A cast128 is an instance of CAST-128 encryption using a particular key.
type cast128 struct {
	xk     []uint32 // Key, after expansion.
	rounds uint     // Number of rounds to use, 12 or 16.
}

// NewCipher creates and returns a new cipher.Block.
// The key argument should be the CAST-128 key (16 bytes).
func NewCipher(key []byte) (cipher.Block, error) {
	k := len(key)
	if k != 16 {
		return nil, KeySizeError(k)
	}

	cipher := &cast128{xk: make([]uint32, 32)}
	cipher.expandKey(key, uint(k))

	return cipher, nil
}

// BlockSize returns the cipher's block size.
func (s *cast128) BlockSize() int {
	return BlockSize
}

// Encrypt encrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s *cast128) Encrypt(dst, src []byte) {
	encrypt(s.xk, s.rounds, dst, src)
}

// Decrypt decrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s *cast128) Decrypt(dst, src []byte) {
	decrypt(s.xk, s.rounds, dst, src)
}

// Functions to access 8-bit bytes out of a 32-bit word.

func u8a(x uint32) byte {
	return byte(x >> 24)
}

func u8b(x uint32) byte {
	return byte((x >> 16) & 255)
}

func u8c(x uint32) byte {
	return byte((x >> 8) & 255)
}

func u8d(x uint32) byte {
	return byte((x) & 255)
}

// Circular left shift.

func rol(x, n uint32) uint32 {
	return (x << n) | (x >> (32 - n))
}
