cast128 [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](https://godoc.org/gitlab.com/opennota/crypto/cast128?status.svg)](http://godoc.org/gitlab.com/opennota/crypto/cast128)
=======

CAST-128 cipher.

## Install

    go get -u gitlab.com/opennota/crypto/cast128
