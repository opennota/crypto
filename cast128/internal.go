// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

package cast128

func encrypt(xk []uint32, rounds uint, dst, src []byte) {
	var t uint32

	// Get inblock into l, r.
	l := (uint32(src[0]) << 24) | (uint32(src[1]) << 16) |
		(uint32(src[2]) << 8) | uint32(src[3])
	r := (uint32(src[4]) << 24) | (uint32(src[5]) << 16) |
		(uint32(src[6]) << 8) | uint32(src[7])

	// Do the work.
	t = rol(xk[0]+r, xk[0+16])
	l ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[1]^l, xk[1+16])
	r ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[2]-r, xk[2+16])
	l ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[3]+l, xk[3+16])
	r ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[4]^r, xk[4+16])
	l ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[5]-l, xk[5+16])
	r ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[6]+r, xk[6+16])
	l ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[7]^l, xk[7+16])
	r ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[8]-r, xk[8+16])
	l ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[9]+l, xk[9+16])
	r ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[10]^r, xk[10+16])
	l ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[11]-l, xk[11+16])
	r ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]

	// Only do full 16 rounds if key length > 80 bits.
	if rounds > 12 {
		t = rol(xk[12]+r, xk[12+16])
		l ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
		t = rol(xk[13]^l, xk[13+16])
		r ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
		t = rol(xk[14]-r, xk[14+16])
		l ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
		t = rol(xk[15]+l, xk[15+16])
		r ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	}

	dst[0] = u8a(r)
	dst[1] = u8b(r)
	dst[2] = u8c(r)
	dst[3] = u8d(r)
	dst[4] = u8a(l)
	dst[5] = u8b(l)
	dst[6] = u8c(l)
	dst[7] = u8d(l)

	// Wipe clean.
	t = 0
	l = 0
	r = 0
}

func decrypt(xk []uint32, rounds uint, dst, src []byte) {
	var t uint32

	// Get inblock into l, r.
	r := (uint32(src[0]) << 24) | (uint32(src[1]) << 16) |
		(uint32(src[2]) << 8) | uint32(src[3])
	l := (uint32(src[4]) << 24) | (uint32(src[5]) << 16) |
		(uint32(src[6]) << 8) | uint32(src[7])

	// Do the work.
	// Only do full 16 rounds if key length > 80 bits.
	if rounds > 12 {
		t = rol(xk[15]+l, xk[15+16])
		r ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
		t = rol(xk[14]-r, xk[14+16])
		l ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
		t = rol(xk[13]^l, xk[13+16])
		r ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
		t = rol(xk[12]+r, xk[12+16])
		l ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	}
	t = rol(xk[11]-l, xk[11+16])
	r ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[10]^r, xk[10+16])
	l ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[9]+l, xk[9+16])
	r ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[8]-r, xk[8+16])
	l ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[7]^l, xk[7+16])
	r ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[6]+r, xk[6+16])
	l ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[5]-l, xk[5+16])
	r ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[4]^r, xk[4+16])
	l ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[3]+l, xk[3+16])
	r ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]
	t = rol(xk[2]-r, xk[2+16])
	l ^= ((cast_sbox1[u8a(t)] + cast_sbox2[u8b(t)]) ^ cast_sbox3[u8c(t)]) - cast_sbox4[u8d(t)]
	t = rol(xk[1]^l, xk[1+16])
	r ^= ((cast_sbox1[u8a(t)] - cast_sbox2[u8b(t)]) + cast_sbox3[u8c(t)]) ^ cast_sbox4[u8d(t)]
	t = rol(xk[0]+r, xk[0+16])
	l ^= ((cast_sbox1[u8a(t)] ^ cast_sbox2[u8b(t)]) - cast_sbox3[u8c(t)]) + cast_sbox4[u8d(t)]

	// Put l, r into outblock.
	dst[0] = u8a(l)
	dst[1] = u8b(l)
	dst[2] = u8c(l)
	dst[3] = u8d(l)
	dst[4] = u8a(r)
	dst[5] = u8b(r)
	dst[6] = u8c(r)
	dst[7] = u8d(r)

	// Wipe clean.
	t = 0
	l = 0
	r = 0
}

func (c *cast128) expandKey(rawkey []byte, keybytes uint) {
	var t, z, x [4]uint32
	var i uint

	// Set number of rounds to 12 or 16, depending on key length.
	if keybytes <= 10 {
		c.rounds = 12
	} else {
		c.rounds = 16
	}

	// Copy key to workspace x.
	for i = 0; i < 4; i++ {
		x[i] = 0
		if i*4+0 < keybytes {
			x[i] = uint32(rawkey[i*4+0]) << 24
		}
		if i*4+1 < keybytes {
			x[i] |= uint32(rawkey[i*4+1]) << 16
		}
		if i*4+2 < keybytes {
			x[i] |= uint32(rawkey[i*4+2]) << 8
		}
		if i*4+3 < keybytes {
			x[i] |= uint32(rawkey[i*4+3])
		}
	}

	// Generate 32 subkeys, four at a time.
	for i = 0; i < 32; i += 4 {
		switch i & 4 {
		case 0:
			t[0] = x[0] ^ cast_sbox5[u8b(x[3])] ^
				cast_sbox6[u8d(x[3])] ^ cast_sbox7[u8a(x[3])] ^
				cast_sbox8[u8c(x[3])] ^
				cast_sbox7[u8a(x[2])]
			z[0] = t[0]
			t[1] = x[2] ^ cast_sbox5[u8a(z[0])] ^
				cast_sbox6[u8c(z[0])] ^ cast_sbox7[u8b(z[0])] ^
				cast_sbox8[u8d(z[0])] ^
				cast_sbox8[u8c(x[2])]
			z[1] = t[1]
			t[2] = x[3] ^ cast_sbox5[u8d(z[1])] ^
				cast_sbox6[u8c(z[1])] ^ cast_sbox7[u8b(z[1])] ^
				cast_sbox8[u8a(z[1])] ^
				cast_sbox5[u8b(x[2])]
			z[2] = t[2]
			t[3] = x[1] ^ cast_sbox5[u8c(z[2])] ^
				cast_sbox6[u8b(z[2])] ^ cast_sbox7[u8d(z[2])] ^
				cast_sbox8[u8a(z[2])] ^
				cast_sbox6[u8d(x[2])]
			z[3] = t[3]
		case 4:
			t[0] = z[2] ^ cast_sbox5[u8b(z[1])] ^
				cast_sbox6[u8d(z[1])] ^ cast_sbox7[u8a(z[1])] ^
				cast_sbox8[u8c(z[1])] ^
				cast_sbox7[u8a(z[0])]
			x[0] = t[0]
			t[1] = z[0] ^ cast_sbox5[u8a(x[0])] ^
				cast_sbox6[u8c(x[0])] ^ cast_sbox7[u8b(x[0])] ^
				cast_sbox8[u8d(x[0])] ^
				cast_sbox8[u8c(z[0])]
			x[1] = t[1]
			t[2] = z[1] ^ cast_sbox5[u8d(x[1])] ^
				cast_sbox6[u8c(x[1])] ^ cast_sbox7[u8b(x[1])] ^
				cast_sbox8[u8a(x[1])] ^
				cast_sbox5[u8b(z[0])]
			x[2] = t[2]
			t[3] = z[3] ^ cast_sbox5[u8c(x[2])] ^
				cast_sbox6[u8b(x[2])] ^ cast_sbox7[u8d(x[2])] ^
				cast_sbox8[u8a(x[2])] ^
				cast_sbox6[u8d(z[0])]
			x[3] = t[3]
		}

		switch i & 12 {
		case 0:
			fallthrough
		case 12:
			c.xk[i+0] =
				cast_sbox5[u8a(t[2])] ^ cast_sbox6[u8b(t[2])] ^
					cast_sbox7[u8d(t[1])] ^
					cast_sbox8[u8c(t[1])]
			c.xk[i+1] =
				cast_sbox5[u8c(t[2])] ^ cast_sbox6[u8d(t[2])] ^
					cast_sbox7[u8b(t[1])] ^
					cast_sbox8[u8a(t[1])]
			c.xk[i+2] =
				cast_sbox5[u8a(t[3])] ^ cast_sbox6[u8b(t[3])] ^
					cast_sbox7[u8d(t[0])] ^
					cast_sbox8[u8c(t[0])]
			c.xk[i+3] =
				cast_sbox5[u8c(t[3])] ^ cast_sbox6[u8d(t[3])] ^
					cast_sbox7[u8b(t[0])] ^
					cast_sbox8[u8a(t[0])]
		case 4:
			fallthrough
		case 8:
			c.xk[i+0] =
				cast_sbox5[u8d(t[0])] ^ cast_sbox6[u8c(t[0])] ^
					cast_sbox7[u8a(t[3])] ^
					cast_sbox8[u8b(t[3])]
			c.xk[i+1] =
				cast_sbox5[u8b(t[0])] ^ cast_sbox6[u8a(t[0])] ^
					cast_sbox7[u8c(t[3])] ^
					cast_sbox8[u8d(t[3])]
			c.xk[i+2] =
				cast_sbox5[u8d(t[1])] ^ cast_sbox6[u8c(t[1])] ^
					cast_sbox7[u8a(t[2])] ^
					cast_sbox8[u8b(t[2])]
			c.xk[i+3] =
				cast_sbox5[u8b(t[1])] ^ cast_sbox6[u8a(t[1])] ^
					cast_sbox7[u8c(t[2])] ^
					cast_sbox8[u8d(t[2])]
		}

		switch i & 12 {
		case 0:
			c.xk[i+0] ^= cast_sbox5[u8c(z[0])]
			c.xk[i+1] ^= cast_sbox6[u8c(z[1])]
			c.xk[i+2] ^= cast_sbox7[u8b(z[2])]
			c.xk[i+3] ^= cast_sbox8[u8a(z[3])]
		case 4:
			c.xk[i+0] ^= cast_sbox5[u8a(x[2])]
			c.xk[i+1] ^= cast_sbox6[u8b(x[3])]
			c.xk[i+2] ^= cast_sbox7[u8d(x[0])]
			c.xk[i+3] ^= cast_sbox8[u8d(x[1])]
		case 8:
			c.xk[i+0] ^= cast_sbox5[u8b(z[2])]
			c.xk[i+1] ^= cast_sbox6[u8a(z[3])]
			c.xk[i+2] ^= cast_sbox7[u8c(z[0])]
			c.xk[i+3] ^= cast_sbox8[u8c(z[1])]
		case 12:
			c.xk[i+0] ^= cast_sbox5[u8d(x[0])]
			c.xk[i+1] ^= cast_sbox6[u8d(x[1])]
			c.xk[i+2] ^= cast_sbox7[u8a(x[2])]
			c.xk[i+3] ^= cast_sbox8[u8b(x[3])]
		}
		if i >= 16 {
			c.xk[i+0] &= 31
			c.xk[i+1] &= 31
			c.xk[i+2] &= 31
			c.xk[i+3] &= 31
		}
	}

	// Wipe clean.
	for i = 0; i < 4; i++ {
		t[i] = 0
		x[i] = 0
		z[i] = 0
	}
}
