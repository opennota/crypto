package saferplus

import (
	"crypto/cipher"
	"encoding/hex"
	"reflect"
	"testing"
)

type testCase struct {
	input  string
	output []byte
}

var testCases = []testCase{
	{
		"0123456789abcdef",
		[]byte{0x8c, 0xaf, 0x9f, 0x17, 0x45, 0x21, 0x19, 0xfc, 0x8d, 0xb0, 0x6f, 0x9d, 0xb4, 0xaa, 0x08, 0xb9},
	},
	{
		"0123456789abcdefghijklmnopqrstuv",
		[]byte{0x8c, 0xaf, 0x9f, 0x17, 0x45, 0x21, 0x19, 0xfc, 0x8d, 0xb0, 0x6f, 0x9d, 0xb4, 0xaa, 0x08, 0xb9, 0x3b, 0x8b, 0xe9, 0x55, 0x69, 0x56, 0x89, 0x32, 0x48, 0x93, 0xcc, 0x38, 0xba, 0x56, 0x48, 0x3f},
	},
}

func TestSaferPlus(t *testing.T) {
	// Self-test from libmcrypt.

	plaintext := make([]byte, 16)
	for i := 0; i < len(plaintext); i++ {
		plaintext[i] = byte(i % 256)
	}

	key := make([]byte, 32)
	for i := 0; i < len(key); i++ {
		key[i] = byte((i*2 + 10) % 256)
	}

	ciphertext := make([]byte, 16)

	block, _ := NewCipher(key)
	block.Encrypt(ciphertext, plaintext)

	const want = "97fa76704bf6b578549f65c6f75b228b"
	got := hex.EncodeToString(ciphertext)
	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}

	plaintext2 := make([]byte, 16)
	block.Decrypt(plaintext2, ciphertext)
	if !reflect.DeepEqual(plaintext, plaintext2) {
		t.Errorf("got %v, want %v", plaintext2, plaintext)
	}

	// Our tests.

	key = make([]byte, 32)
	for i := 0; i < len(key); i++ {
		key[i] = byte(i)
	}

	iv := make([]byte, 16)
	for i := 0; i < len(iv); i++ {
		iv[i] = byte(i)
	}

	block, _ = NewCipher(key)

	for _, tc := range testCases {
		cbc := cipher.NewCBCEncrypter(block, iv)
		text := []byte(tc.input)
		cbc.CryptBlocks(text, text)

		if !reflect.DeepEqual(text, tc.output) {
			t.Errorf("got %v, want %v", text, tc.output)
		}

		dec := cipher.NewCBCDecrypter(block, iv)
		text2 := make([]byte, len(text))
		dec.CryptBlocks(text2, text)

		if !reflect.DeepEqual(text2, []byte(tc.input)) {
			t.Errorf("got %v, want %v", text2, []byte(tc.input))
		}
	}
}
