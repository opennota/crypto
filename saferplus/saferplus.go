// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package saferplus implements SAFER+ encryption.
package saferplus

import (
	"crypto/cipher"
	"reflect"
	"strconv"
	"unsafe"
)

// BlockSize is the SAFER+ block size in bytes.
const BlockSize = 16

type KeySizeError int

func (k KeySizeError) Error() string {
	return "safer+: invalid key size " + strconv.Itoa(int(k))
}

// A saferplus is an instance of SAFER+ encryption using a particular key.
type saferplus struct {
	xk      []byte
	k_bytes uint32
}

// NewCipher creates and returns a new cipher.Block.
// The key argument should be the SAFER+ key,
// either 16, 24, or 32 bytes to select
// SAFER+-128, SAFER+-192, or SAFER+-256.
func NewCipher(key []byte) (cipher.Block, error) {
	k := len(key)
	switch k {
	case 16, 24, 32:
		break
	default:
		return nil, KeySizeError(k)
	}

	cipher := &saferplus{xk: make([]byte, 33*16)}
	cipher.expandKey(byteSliceToUint32Slice(key), uint32(k))

	return cipher, nil
}

// BlockSize returns the cipher's block size.
func (s *saferplus) BlockSize() int {
	return BlockSize
}

// Encrypt encrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s *saferplus) Encrypt(dst, src []byte) {
	encrypt(s.xk, s.k_bytes, byteSliceToUint32Slice(dst), byteSliceToUint32Slice(src))
}

// Decrypt decrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s *saferplus) Decrypt(dst, src []byte) {
	decrypt(s.xk, s.k_bytes, byteSliceToUint32Slice(dst), byteSliceToUint32Slice(src))
}

func uint32SliceToByteSlice(s []uint32) []byte {
	header := *(*reflect.SliceHeader)(unsafe.Pointer(&s))
	header.Cap *= 4
	header.Len *= 4
	return *(*[]byte)(unsafe.Pointer(&header))
}

func byteSliceToUint32Slice(s []byte) []uint32 {
	header := *(*reflect.SliceHeader)(unsafe.Pointer(&s))
	header.Cap /= 4
	header.Len /= 4
	return *(*[]uint32)(unsafe.Pointer(&header))
}
