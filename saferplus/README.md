saferplus [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](https://godoc.org/gitlab.com/opennota/crypto/saferplus?status.svg)](http://godoc.org/gitlab.com/opennota/crypto/saferplus)
=========

SAFER+ cipher.

## Install

    go get -u gitlab.com/opennota/crypto/saferplus
