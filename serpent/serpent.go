// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package serpent implements Serpent encryption.
package serpent

//go:generate macro internal.go.tmpl internal.go

import (
	"crypto/cipher"
	"strconv"
)

// BlockSize is the Serpent block size in bytes.
const BlockSize = 16

type KeySizeError int

func (k KeySizeError) Error() string {
	return "serpent: invalid key size " + strconv.Itoa(int(k))
}

// A serpent is an instance of Serpent encryption using a particular key.
type serpent []uint32

// NewCipher creates and returns a new cipher.Block.
// The key argument should be the Serpent key,
// either 16, 24, or 32 bytes to select
// Serpent-128, Serpent-192, or Serpent-256.
func NewCipher(key []byte) (cipher.Block, error) {
	k := len(key)
	switch k {
	case 16, 24, 32:
		break
	default:
		return nil, KeySizeError(k)
	}

	cipher := make(serpent, 140)
	expandKey(cipher, key, uint32(k)*8)

	return cipher, nil
}

// BlockSize returns the cipher's block size.
func (s serpent) BlockSize() int {
	return BlockSize
}

// Encrypt encrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s serpent) Encrypt(dst, src []byte) {
	encrypt(s, dst, src)
}

// Decrypt decrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s serpent) Decrypt(dst, src []byte) {
	decrypt(s, dst, src)
}

func rotl32(x, n uint32) uint32 {
	return (x << n) | (x >> (32 - n))
}

func rotr32(x, n uint32) uint32 {
	return (x >> n) | (x << (32 - n))
}
