package serpent

import (
	"crypto/cipher"
	"encoding/hex"
	"reflect"
	"testing"
)

type testCase struct {
	input  string
	output []byte
}

var testCases = []testCase{
	{
		"0123456789abcdef",
		[]byte{0x93, 0x33, 0xf5, 0x29, 0x54, 0x7f, 0x0c, 0x9a, 0xb5, 0xc7, 0x70, 0xd3, 0x96, 0x2e, 0x86, 0x59},
	},
	{
		"0123456789abcdefghijklmnopqrstuv",
		[]byte{0x93, 0x33, 0xf5, 0x29, 0x54, 0x7f, 0x0c, 0x9a, 0xb5, 0xc7, 0x70, 0xd3, 0x96, 0x2e, 0x86, 0x59,
			0x75, 0x25, 0x8b, 0x00, 0x10, 0xed, 0xd5, 0x32, 0xc5, 0x45, 0xf1, 0xeb, 0x14, 0xaf, 0xde, 0x73},
	},
}

func TestSerpent(t *testing.T) {
	// Self-test from libmcrypt.

	plaintext := make([]byte, 16)
	for i := 0; i < len(plaintext); i++ {
		plaintext[i] = byte(i % 256)
	}

	key := make([]byte, 32)
	for i := 0; i < len(key); i++ {
		key[i] = byte((i*2 + 10) % 256)
	}

	ciphertext := make([]byte, 16)

	block, _ := NewCipher(key)
	block.Encrypt(ciphertext, plaintext)

	const want = "9a99455df5080bfccadf049b5aaf7d61"
	got := hex.EncodeToString(ciphertext)
	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}

	plaintext2 := make([]byte, 16)
	block.Decrypt(plaintext2, ciphertext)
	if !reflect.DeepEqual(plaintext, plaintext2) {
		t.Errorf("got %v, want %v", plaintext2, plaintext)
	}

	// Our tests.

	key = make([]byte, 32)
	for i := 0; i < len(key); i++ {
		key[i] = byte(i)
	}

	iv := make([]byte, 16)
	for i := 0; i < len(iv); i++ {
		iv[i] = byte(i)
	}

	block, _ = NewCipher(key)

	for _, tc := range testCases {
		cbc := cipher.NewCBCEncrypter(block, iv)
		text := []byte(tc.input)
		cbc.CryptBlocks(text, text)

		if !reflect.DeepEqual(text, tc.output) {
			t.Errorf("got %v, want %v", text, tc.output)
		}

		dec := cipher.NewCBCDecrypter(block, iv)
		text2 := make([]byte, len(text))
		dec.CryptBlocks(text2, text)

		if !reflect.DeepEqual(text2, []byte(tc.input)) {
			t.Errorf("got %v, want %v", text2, []byte(tc.input))
		}
	}
}
