package cast256

import (
	"crypto/cipher"
	"encoding/hex"
	"reflect"
	"testing"
)

type testCase struct {
	input  string
	output []byte
}

var testCases = []testCase{
	{
		"0123456789abcdef",
		[]byte{0x47, 0x7f, 0x48, 0xa1, 0xb1, 0x6d, 0xf2, 0xb8, 0xdf, 0x58, 0x5f, 0xa9, 0xf5, 0x65, 0xda, 0x85},
	},
	{
		"0123456789abcdefghijklmnopqrstuv",
		[]byte{0x47, 0x7f, 0x48, 0xa1, 0xb1, 0x6d, 0xf2, 0xb8, 0xdf, 0x58, 0x5f, 0xa9, 0xf5, 0x65, 0xda, 0x85,
			0xa5, 0x0e, 0x76, 0xf0, 0x38, 0x62, 0x2e, 0x85, 0xb7, 0xa1, 0x84, 0x28, 0x28, 0x4a, 0x7f, 0xba},
	},
}

func TestCast256(t *testing.T) {
	// Self-test from libmcrypt.

	plaintext := make([]byte, 16)
	for i := 0; i < len(plaintext); i++ {
		plaintext[i] = byte(i % 256)
	}

	key := make([]byte, 32)
	for i := 0; i < len(key); i++ {
		key[i] = byte((i*2 + 10) % 256)
	}

	ciphertext := make([]byte, 16)

	block, _ := NewCipher(key)
	block.Encrypt(ciphertext, plaintext)

	const want = "5db4dd765f1d3835615a14afcb5dc2f5"
	got := hex.EncodeToString(ciphertext)
	if got != want {
		t.Errorf("got %q, want %q", got, want)
	}

	plaintext2 := make([]byte, 16)
	block.Decrypt(plaintext2, ciphertext)
	if !reflect.DeepEqual(plaintext, plaintext2) {
		t.Errorf("got %v, want %v", plaintext2, plaintext)
	}

	// Our tests.

	key = make([]byte, 32)
	for i := 0; i < len(key); i++ {
		key[i] = byte(i)
	}

	iv := make([]byte, 16)
	for i := 0; i < len(iv); i++ {
		iv[i] = byte(i)
	}

	block, _ = NewCipher(key)

	for _, tc := range testCases {
		cbc := cipher.NewCBCEncrypter(block, iv)
		text := []byte(tc.input)
		cbc.CryptBlocks(text, text)

		if !reflect.DeepEqual(text, tc.output) {
			t.Errorf("got %v, want %v", text, tc.output)
		}

		cbcdec := cipher.NewCBCDecrypter(block, iv)
		text2 := make([]byte, len(text))
		cbcdec.CryptBlocks(text2, text)

		if !reflect.DeepEqual(text2, []byte(tc.input)) {
			t.Errorf("got %v, want %v", text2, []byte(tc.input))
		}
	}
}
