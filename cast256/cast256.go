// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package cast256 implements CAST-256 encryption.
package cast256

//go:generate macro -r internal.go.tmpl internal.go

import (
	"crypto/cipher"
	"reflect"
	"strconv"
	"unsafe"
)

// BlockSize is the CAST-256 block size in bytes.
const BlockSize = 16

type KeySizeError int

func (k KeySizeError) Error() string {
	return "cast256: invalid key size " + strconv.Itoa(int(k))
}

// A cast256 is an instance of CAST-256 encryption using a particular key.
type cast256 struct {
	xk []uint32
}

// NewCipher creates and returns a new cipher.Block.
// The key argument should be the CAST-256 key (32 bytes).
func NewCipher(key []byte) (cipher.Block, error) {
	k := len(key)
	if k != 32 {
		return nil, KeySizeError(k)
	}

	cipher := &cast256{make([]uint32, 96)}
	expandKey(cipher.xk, byteSliceToUint32Slice(key), uint32(k))

	return cipher, nil
}

// BlockSize returns the cipher's block size.
func (s *cast256) BlockSize() int {
	return BlockSize
}

// Encrypt encrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s *cast256) Encrypt(dst, src []byte) {
	encrypt(s.xk, byteSliceToUint32Slice(dst), byteSliceToUint32Slice(src))
}

// Decrypt decrypts the first block in src into dst.
// Dst and src may point at the same memory.
func (s *cast256) Decrypt(dst, src []byte) {
	decrypt(s.xk, byteSliceToUint32Slice(dst), byteSliceToUint32Slice(src))
}

func byten(x, n uint32) byte {
	return byte((x >> (8 * n)) & 0xff)
}

func rotl32(x, n uint32) uint32 {
	n &= 31
	return (x << n) | (x >> (32 - n))
}

func rotr32(x, n uint32) uint32 {
	n &= 31
	return (x >> n) | (x << (32 - n))
}

func byteSliceToUint32Slice(s []byte) []uint32 {
	header := *(*reflect.SliceHeader)(unsafe.Pointer(&s))
	header.Cap /= 4
	header.Len /= 4
	return *(*[]uint32)(unsafe.Pointer(&header))
}

func uint32SliceToByteSlice(s []uint32) []byte {
	header := *(*reflect.SliceHeader)(unsafe.Pointer(&s))
	header.Cap *= 4
	header.Len *= 4
	return *(*[]byte)(unsafe.Pointer(&header))
}

func byteswap32(x uint32) uint32 {
	return (rotl32(x, 8) & 0x00ff00ff) | (rotr32(x, 8) & 0xff00ff00)
}

var bigEndian bool

func init() {
	x := uint32(0x04030201)
	y := [4]byte{0x4, 0x3, 0x2, 0x1}
	bigEndian = *(*[4]byte)(unsafe.Pointer(&x)) == y
}
